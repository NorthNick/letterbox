# Letterbox
Letterbox is a C# program to help play the final round of the BBC2 Letterbox game show.

The object is to guess an 8-letter word in which every letter is different.
You are first given 8 letters that are not in the word to be guessed.
Each time you guess a letter, you are either told that it is not in the word (and lose £500 prize money),
or you are told where it appears in the word.
When you get the word right, you receive however much of the initial £2,500 prize money is left.

Use the program as follows:

* Set the length of words you want to play with, defaulting to 8, and click Set up.
* Enter the initial excluded letters in the Exclusions text boxes. As each is entered the number
of remaining words is updated, the first candidate are shown in the text box, and remaining letters
are shown in decreasing order of frequency, so that you know what the best guess would be.
* As letters are guessed, enter them in the Exclusions boxes, or in the right Matches box as appropriate.
The disaply is updated to match.
* Once the result is known, you can rerun the game, selecting letters in decreasing frequency order, to see
if that finds the results more quickly.

## Acknowledgements

The wordlist comes from [Mieliestronk](http://www.mieliestronk.com/wordlist.html)