﻿namespace Letterbox
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.LengthBox = new System.Windows.Forms.NumericUpDown();
            this.CountLabel = new System.Windows.Forms.Label();
            this.SetUpBtn = new System.Windows.Forms.Button();
            this.CandidatesBox = new System.Windows.Forms.TextBox();
            this.FrequencyLabel = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.LengthBox)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Word length:";
            // 
            // LengthBox
            // 
            this.LengthBox.Location = new System.Drawing.Point(87, 11);
            this.LengthBox.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.LengthBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.LengthBox.Name = "LengthBox";
            this.LengthBox.Size = new System.Drawing.Size(35, 20);
            this.LengthBox.TabIndex = 1;
            this.LengthBox.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // CountLabel
            // 
            this.CountLabel.AutoSize = true;
            this.CountLabel.Location = new System.Drawing.Point(138, 13);
            this.CountLabel.Name = "CountLabel";
            this.CountLabel.Size = new System.Drawing.Size(93, 13);
            this.CountLabel.TabIndex = 2;
            this.CountLabel.Text = "Number of words: ";
            // 
            // SetUpBtn
            // 
            this.SetUpBtn.Location = new System.Drawing.Point(16, 48);
            this.SetUpBtn.Name = "SetUpBtn";
            this.SetUpBtn.Size = new System.Drawing.Size(75, 23);
            this.SetUpBtn.TabIndex = 3;
            this.SetUpBtn.Text = "Set up";
            this.SetUpBtn.UseVisualStyleBackColor = true;
            this.SetUpBtn.Click += new System.EventHandler(this.SetUpBtn_Click);
            // 
            // CandidatesBox
            // 
            this.CandidatesBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CandidatesBox.Location = new System.Drawing.Point(16, 241);
            this.CandidatesBox.Multiline = true;
            this.CandidatesBox.Name = "CandidatesBox";
            this.CandidatesBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.CandidatesBox.Size = new System.Drawing.Size(415, 246);
            this.CandidatesBox.TabIndex = 4;
            // 
            // FrequencyLabel
            // 
            this.FrequencyLabel.AutoSize = true;
            this.FrequencyLabel.Location = new System.Drawing.Point(13, 207);
            this.FrequencyLabel.Name = "FrequencyLabel";
            this.FrequencyLabel.Size = new System.Drawing.Size(106, 13);
            this.FrequencyLabel.TabIndex = 5;
            this.FrequencyLabel.Text = "Most frequent letters:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(16, 170);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(21, 20);
            this.textBox1.TabIndex = 6;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(43, 170);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(21, 20);
            this.textBox2.TabIndex = 7;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(70, 170);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(21, 20);
            this.textBox3.TabIndex = 8;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(97, 170);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(21, 20);
            this.textBox4.TabIndex = 9;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(124, 170);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(21, 20);
            this.textBox5.TabIndex = 10;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(151, 170);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(21, 20);
            this.textBox6.TabIndex = 11;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(178, 170);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(21, 20);
            this.textBox7.TabIndex = 12;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(205, 170);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(21, 20);
            this.textBox8.TabIndex = 13;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(232, 170);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(21, 20);
            this.textBox9.TabIndex = 14;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(259, 170);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(21, 20);
            this.textBox10.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 151);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Matches";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(259, 117);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(21, 20);
            this.textBox11.TabIndex = 26;
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(232, 117);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(21, 20);
            this.textBox12.TabIndex = 25;
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(205, 117);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(21, 20);
            this.textBox13.TabIndex = 24;
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(178, 117);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(21, 20);
            this.textBox14.TabIndex = 23;
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(151, 117);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(21, 20);
            this.textBox15.TabIndex = 22;
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(124, 117);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(21, 20);
            this.textBox16.TabIndex = 21;
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(97, 117);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(21, 20);
            this.textBox17.TabIndex = 20;
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(70, 117);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(21, 20);
            this.textBox18.TabIndex = 19;
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(43, 117);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(21, 20);
            this.textBox19.TabIndex = 18;
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(16, 117);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(21, 20);
            this.textBox20.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "Exclusions";
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(313, 117);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(21, 20);
            this.textBox21.TabIndex = 29;
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(286, 117);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(21, 20);
            this.textBox22.TabIndex = 28;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 502);
            this.Controls.Add(this.textBox21);
            this.Controls.Add(this.textBox22);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox11);
            this.Controls.Add(this.textBox12);
            this.Controls.Add(this.textBox13);
            this.Controls.Add(this.textBox14);
            this.Controls.Add(this.textBox15);
            this.Controls.Add(this.textBox16);
            this.Controls.Add(this.textBox17);
            this.Controls.Add(this.textBox18);
            this.Controls.Add(this.textBox19);
            this.Controls.Add(this.textBox20);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.textBox9);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.FrequencyLabel);
            this.Controls.Add(this.CandidatesBox);
            this.Controls.Add(this.SetUpBtn);
            this.Controls.Add(this.CountLabel);
            this.Controls.Add(this.LengthBox);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Letterbox";
            ((System.ComponentModel.ISupportInitialize)(this.LengthBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown LengthBox;
        private System.Windows.Forms.Label CountLabel;
        private System.Windows.Forms.Button SetUpBtn;
        private System.Windows.Forms.TextBox CandidatesBox;
        private System.Windows.Forms.Label FrequencyLabel;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox22;
    }
}

