﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Letterbox
{
    public partial class Form1 : Form
    {
        private const string WordsFile = "AllWords.txt";
        private const int MaxCandidates = 100;
        private List<string> _words;
        private int _wordLength;
        private readonly TextBox[] _choices, _exclusions;

        public Form1()
        {
            InitializeComponent();
            _choices = new[] { textBox1, textBox2, textBox3, textBox4, textBox5, textBox6, textBox7, textBox8, textBox9, textBox10 };
            _exclusions = new[] { textBox11, textBox12, textBox13, textBox14, textBox15, textBox16, textBox17, textBox18, textBox19, textBox20, textBox21, textBox22 };
            foreach (var box in _choices.Concat(_exclusions)) {
                box.TextChanged += OnTextChanged;
            }
        }

        private void SetUpBtn_Click(object sender, EventArgs e)
        {
            _wordLength = (int)LengthBox.Value;
            _words = System.IO.File.ReadAllLines(WordsFile).Where(word => word.Length == _wordLength && Unique(word)).OrderBy(word => word).ToList();
            for (int i = 0; i < _choices.Length; i++) {
                _choices[i].Visible = i < _wordLength;
            }
            ShowResults(_words);
        }

        private void ShowResults(List<string> words)
        {
            CountLabel.Text = $"Number of words: {words.Count}";
            SetCandidates(words);
            SetFrequencies(words, _choices);
        }

        private static bool Unique(string word)
        {
            // Find if word has any repeated letters
            return word.Distinct().Count() == word.Length;
        }

        private static bool Excluded(string word, HashSet<char> excluded)
        {
            return word.Any(excluded.Contains);
        }

        private void SetFrequencies(List<string> words, TextBox[] choices)
        {
            var chosen = new HashSet<char>(choices.Where(box => !string.IsNullOrEmpty(box.Text)).SelectMany(box => box.Text.ToUpper()));
            var freq = new string(words.SelectMany(c => c).Where(c => !chosen.Contains(c)).
                GroupBy(c => c).OrderByDescending(gp => gp.Count()).Select(gp => gp.Key).ToArray());
            FrequencyLabel.Text = $"Most frequent letters: {freq}";
        }

        private void SetCandidates(List<string> words)
        {
            CandidatesBox.Text = string.Join(Environment.NewLine, words.Take(MaxCandidates));
        }

        private void OnTextChanged(object sender, EventArgs e)
        {
            string mask = new string(_choices.Take(_wordLength).Select(box => string.IsNullOrEmpty(box.Text) ? '.' : box.Text.ToUpper().First()).ToArray());
            var pattern = new Regex(mask, RegexOptions.Compiled);
            var excluded = new HashSet<char>(_exclusions.Where(box => !string.IsNullOrEmpty(box.Text)).Select(box => box.Text.ToUpper().First()));
            var filteredWords = _words.Where(word => !Excluded(word, excluded) && pattern.IsMatch(word)).ToList();
            ShowResults(filteredWords);
        }

    }
}
